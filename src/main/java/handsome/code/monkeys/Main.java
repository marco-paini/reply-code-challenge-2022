package handsome.code.monkeys;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.validation.Valid;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;
import org.optaplanner.core.api.solver.SolverJob;
import org.optaplanner.core.api.solver.SolverManager;

import handsome.code.monkeys.data.Demon;
import handsome.code.monkeys.optaplanner.Solution;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import lombok.SneakyThrows;

@QuarkusMain
public class Main implements QuarkusApplication {
	@ConfigProperty
	Optional<List<String>> files;

	@ConfigProperty(defaultValue = "6")
	long limit;

	@Inject
	Logger log;

	@ConfigProperty(defaultValue = "false")
	boolean logSolution;

	@ConfigProperty(defaultValue = "0")
	long skip;

	@Inject
	SolverManager<Solution, Path> solverManager;

	private String input(final String string) {
		return String.format("%s/src/main/resources/input/%s", System.getProperty("user.dir"), string);
	}

	@SneakyThrows
	private Stream<String> inputs() {
		return Files.list(Paths.get(String.format("%s/src/main/resources/input", System.getProperty("user.dir"))))
				.map(Path::getFileName).map(Path::toString);
	}

	Solution isValid(@Valid final Solution solution) {
		return solution;
	}

	private String output(final Path path) {
		return String.format("%s/src/main/resources/output/%s", System.getProperty("user.dir"), path);
	}

	@Override
	public int run(String... args) throws Exception {
		files.map(List::stream).orElseGet(this::inputs).skip(skip).limit(limit).map(this::input).map(Paths::get)
				.map(this::solve).forEach(this::write);
		return 0;
	}

	private SolverJob<Solution, Path> solve(final Path path) {
		return solverManager.solve(path, isValid(new Solution(path)));
	}

	@SneakyThrows
	private void write(final SolverJob<Solution, Path> solverJob) {
		final Path path = solverJob.getProblemId().getFileName();
		final Solution solution = solverJob.getFinalBestSolution();
		log.infof("=== %s ===", path);
		if (logSolution) {
			log.info(solution);
		}
		Files.write(Paths.get(output(path)),
				solution.getE().stream()
						.sorted(Comparator.comparing(Demon::getTn, Comparator.nullsLast(Comparator.naturalOrder())))
						.map(Demon::getId).map(String::valueOf).collect(Collectors.toList()));
	}
}
