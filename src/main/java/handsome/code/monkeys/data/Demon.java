package handsome.code.monkeys.data;

import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.lookup.PlanningId;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@PlanningEntity
public class Demon {
	/**
	 * Amount of fragments you’ll earn in the i-th subsequent turn after the fight
	 */
	@Max(10000)
	@Min(0)
	private final List<Integer> A;

	@PlanningId
	private final Integer id;

	/** Number of turns you’ll earn fragments after defeating the demon */
	@Max(100000)
	@Min(0)
	private final Integer Na;

	/**
	 * Amount of stamina points you consume to face the demon; you cannot face a
	 * demon if you don’t have at least this amount of stamina
	 */
	@Max(100000)
	@Min(0)
	private final Integer Sc;

	/** Amount of stamina that you recover after Tr turns have passed */
	@Max(100000)
	@Min(0)
	private final Integer Sr;

	/** Turn in which the n-th enemy is defeated */
	@PlanningVariable(nullable = true, valueRangeProviderRefs = { "Tn" })
	private Integer Tn;

	/** Number of turns you have to wait before recovering stamina */
	@Min(1)
	private final Integer Tr;
}
