package handsome.code.monkeys.optaplanner;

import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;
import org.optaplanner.core.api.score.stream.Constraint;
import org.optaplanner.core.api.score.stream.ConstraintCollectors;
import org.optaplanner.core.api.score.stream.ConstraintFactory;
import org.optaplanner.core.api.score.stream.ConstraintProvider;
import org.optaplanner.core.api.score.stream.Joiners;

import handsome.code.monkeys.data.Demon;
import handsome.code.monkeys.optaplanner.Solution.ProblemFact;

public class SolutionConstraintProvider implements ConstraintProvider {
	@Override
	public Constraint[] defineConstraints(ConstraintFactory constraintFactory) {
		return new Constraint[] { penalizeS(constraintFactory), penalizeT(constraintFactory),
				rewardR(constraintFactory) };
	}

	private Constraint penalizeS(final ConstraintFactory constraintFactory) {
		return constraintFactory
				.forEachUniquePair(Demon.class,
						Joiners.greaterThan(Demon::getTn))
				.groupBy((d1,
						d2) -> d1, ConstraintCollectors
								.toList((d1, d2) -> d2))
				.join(ProblemFact.class)
				.filter((d, ds,
						pf) -> ds.stream().reduce(pf.getSi(),
								(s, d2) -> Math.min(
										s - d2.getSc() + (d2.getTn() + d2.getTr() > d.getTn() ? 0 : d2.getSr()),
										pf.getSmax()),
								Integer::sum) - d.getSc() < 0)
				.penalize("S", HardSoftLongScore.ONE_HARD);
	}

	private Constraint penalizeT(final ConstraintFactory constraintFactory) {
		return constraintFactory.forEachUniquePair(Demon.class, Joiners.equal(Demon::getTn)).penalize("T",
				HardSoftLongScore.ONE_HARD);
	}

	private Constraint rewardR(final ConstraintFactory constraintFactory) {
		return constraintFactory.forEach(Demon.class).join(ProblemFact.class).reward("R", HardSoftLongScore.ONE_SOFT,
				(d, pf) -> d.getA().stream().limit(pf.getT() - d.getTn()).mapToInt(Integer::intValue).sum());
	}
}
