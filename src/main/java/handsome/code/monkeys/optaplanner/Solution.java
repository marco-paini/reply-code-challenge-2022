package handsome.code.monkeys.optaplanner;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.ProblemFactProperty;
import org.optaplanner.core.api.domain.valuerange.CountableValueRange;
import org.optaplanner.core.api.domain.valuerange.ValueRangeFactory;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;

import handsome.code.monkeys.data.Demon;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.Value;

@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@PlanningSolution
public class Solution {
	@Value
	class ProblemFact {
		Integer Si;

		Integer Smax;

		Integer T;
	}

	/** Number of demons available */
	@Max(100000)
	private Integer D;

	@PlanningEntityCollectionProperty
	private List<Demon> E = new ArrayList<>();

	@PlanningScore
	private HardSoftLongScore score;

	/** Amount of stamina the player starts with */
	@Max(100000)
	@Min(0)
	private Integer Si;

	/** Maximum amount of stamina you can cumulate during the fight */
	@Max(100000)
	@Min(0)
	private Integer Smax;

	/** Number of turns available */
	@Max(1000000)
	private Integer T;

	@SneakyThrows
	public Solution(final Path path) {
		Files.lines(path).forEach(line -> {
			final List<Integer> numbers = Stream.of(line.split(" ")).map(Integer::valueOf).collect(Collectors.toList());
			if (D == null) {
				D = numbers.get(3);
				Si = numbers.get(0);
				Smax = numbers.get(1);
				T = numbers.get(2);
			} else if (E.size() < D) {
				E.add(new Demon(numbers.subList(4, numbers.size()), E.size(), numbers.get(3), numbers.get(0),
						numbers.get(2), numbers.get(1)));
			}
		});
		// solve();
	}

	@ProblemFactProperty
	public ProblemFact getProblemFact() {
		return new ProblemFact(Si, Smax, T);
	}

	@ValueRangeProvider(id = "Tn")
	public CountableValueRange<Integer> getTn() {
		return ValueRangeFactory.createIntValueRange(0, T);
	}

	private void solve() {
		List<Demon> ds = E;
		int si = Si;
		int tn = T;
		int tl = 0;
		List<Map.Entry<Integer, Integer>> recovery = new ArrayList<>();
		while (tn > 0) {
			final int s = si;
			final int t = tn;
			ds = ds.stream()
					.sorted(Comparator.comparing(d -> d.getA().stream().limit(t).mapToInt(Integer::valueOf).sum(),
							Comparator.naturalOrder()))
					.collect(Collectors.toList());
			Demon de = ds.stream().filter(d -> s >= d.getSc()).findFirst().orElse(null);
			if (de == null) {
				if (recovery.isEmpty()) {
					break;
				}
				final Map.Entry<Integer, Integer> rec = recovery.stream()
						.sorted(Comparator.comparing(e -> e.getKey(), Comparator.naturalOrder())).findFirst()
						.orElse(null);
				si = Math.min(si + rec.getValue(), Smax);
				tn -= rec.getKey();
				tl += rec.getKey();
				recovery.remove(rec);
				for (Map.Entry<Integer, Integer> e : recovery.stream().collect(Collectors.toList())) {
					recovery.remove(e);
					final Integer i = e.getKey() - tn;
					if (i == 0) {
						si = Math.min(si + e.getValue(), Smax);
					} else {
						recovery.add(Map.entry(i, e.getValue()));
					}
				}
				continue;
			}
			de.setTn(tl);
			si -= de.getSc();
			recovery.add(Map.entry(de.getTr(), de.getSr()));
			tn--;
			tl++;
			for (Map.Entry<Integer, Integer> e : recovery.stream().collect(Collectors.toList())) {
				recovery.remove(e);
				final Integer i = e.getKey() - 1;
				if (i == 0) {
					si = Math.min(si + e.getValue(), Smax);
				} else {
					recovery.add(Map.entry(i, e.getValue()));
				}
			}
			ds.remove(de);
		}
	}
}
